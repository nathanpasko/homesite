module.exports = {
  siteMetadata: {
    title: "ghomesite",
  },
  plugins: [
    `gatsby-plugin-provide-react`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-react-helmet`
  ],
};
