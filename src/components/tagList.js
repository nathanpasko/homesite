import { Link } from "gatsby";

function TagList(props) {
  console.log(props);
  var list = props.tags.map((tag, key) => {
    let t = tag.replace(/\s+/g, "-");
    t = t.replace(/'/g, "");
    return (
      <li key={key}>
        <Link to={`/blog/tags/${t}`}>{tag}</Link>
      </li>
    );
  });

  return <ul className="tag-list">{list}</ul>;
}

export default TagList;
