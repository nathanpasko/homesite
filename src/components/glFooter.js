function GlFooter(props) {
  return (
    <footer id="gl-footer">
      <p className="centered">&copy; {new Date().getFullYear()} Nathan Pasko</p>
    </footer>
  );
}

export default GlFooter;