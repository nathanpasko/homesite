import { graphql } from "gatsby";
import Layout from "../components/layout";

export default function ProjectPage({ data }) {
  const post = data.markdownRemark;
  const frontmatter = post.frontmatter;

  return (
    <Layout>
      <div className="project-page">      
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    </Layout>
  );
}