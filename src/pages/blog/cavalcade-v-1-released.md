---
title: "Cavalcade v1.0 -- Released"
date: "2018-10-27"
tags: ["cavalcade", "devlog", "gamedev"]
---

![screenshot of lumpy creature](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTUucG5n/original/d5w2D2.png)
![screenshot of creature with feet and a candle](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTMucG5n/x200/ZjdpUA.png)

cavalcade v1.0

Happy Halloween!

Cavalcade is available now. This short creepy game takes about 5-10 minutes to explore. Cavalcade is about atmosphere, so take a deep breath and immerse yourself. There are 12 rooms to see in version 1.0.