---
title: "Mech Visualizer"
date: "2020-03-03"
tags: ["future battle legend", "devlog", "gamedev"]
---

With the new _Future Battle Legend_ now in development, a new devlog series begins alongside. The game is made with Unity, coded in C#, and modeled in Blender. In this first post I’ll talk about the earliest steps in the game’s development: the data structure of a Mech and building a system to render a Mech blueprint onscreen.

The main characters of _Future Battle Legend_ are all skilled Mech pilots. Amidst other gameplay like walking around town, using the calendar or a tournament bracket, Mech building and battling take center stage. The blueprint for a Mech is called a Plan and it details the entire build. We think about the bipedal Mech cavalry as a series of **Columns**:

**Port**
- Shoulder
- Forearm
- Hand
- Thigh
- Shin
- Foot

**Starboard**
- Shoulder
- Forearm
- Hand
- Thigh
- Shin
- Foot

**Center**
- Torso
- Head

Each of these Columns contains a number of Parts. Pilots can buy Parts outright or build their own if they have the equipment. The Part count for each Column and the order the Parts are attached in is up to the player, with a few small exceptions. Parts also have their own list of Stats.

**Stats**
- Condition
- Agility
- Boost
- Armor
- Tech
- Weight

Every Part in the game has this same Stat block, and the Mech Plan itself can pull a block of Stat totals from every Part in every Column. This will be necessary for determining a Mech's Boost power or ground speed. Abilities (i.e. attacks) sometimes appear on Parts as well.

For this early development stage, we're basically just working on a prototype. But we want to be able to roll some of our scripts over into the final game, so we begin working on a MonoBehaviour called **MechVisualizer**. We should be able to feed MechVisualizer a Plan and have it serve up a 3D representation of the described Mech onscreen. This could be used as a basis for any Mech we need to render in the game. The scripts then added on top of the MechVisualizer would change from situation to situation: for a fight, we would add fight controls; for a menu screen, we might add only animation; for an interlude when the Mech is shutdown, we could add nothing and leave it in the background.

There have been many solutions to databases in games over the years; in this case, data about every Part, every Ability, and so on must be in a database somewhere in the game because we need to be able manufacture non-unique instances of those objects willy-nilly. The MechVisualizer will create the GameObjects corresponding to each Part, but the reference data for every Part must be pulled from the **Catalog**. This ScriptableObject maintains access to the Part database and prefabs of every Part so we can instantiate them. To funnel spreadsheets into Unity I could [download Robert Yang's **Bobbin**](https://github.com/radiatoryang/bobbin) and limit myself to typing the database a single time. Of course, since the Parts in the Catalog sometimes have Abilities attached to them, the **Playbook**, a similar datastore detailing Abilities, has to initialize first.

The placement of the Parts is trickier than their creation. The main problem is the customization afforded the player in _Future Battle Legend_. Since the length of any one Column is fluid, and many Columns need to physically hang off of one another, MechVisualizer must adjust the origin points of each Column. A long shoulder Column would force the connecting forearm Column, and in turn, the hand, to move from their default position. In the scene, the MechVisualizer attaches Part data to MonoBehaviours called HardParts that govern the individual instances. After instantiating all Parts from the Plan, it hangs the transforms of each Part on the correct Column, and moves the bone lengthwise along the column according to the length of the preceding Parts. The fluid positioning of Columns was a tricky task for me, and what still remains is moving Columns the correct distance from the "ground", such as in the case of long arms or long legs.

The next step in building up _Future Battle Legend_ will be to take the MechVisualizer as a base, and attach the onscreen Mech to an armature, allowing animation. With an Animator in the mix, we can start hashing out how a Mech will move in combat. Thanks for reading! For more updates on this game's development [follow @avknights_dev on Twitter](https://twitter.com/avknights_dev).

