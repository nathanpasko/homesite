---
title: "Fight Control Basis"
date: "2020-03-08"
tags: ["future battle legend", "devlog", "gamedev"]
---

In the world of _Future Battle Legend_, mech battling is a spectator sport with a massive fanbase. Tens of thousands of fans will crowd into the stadium to watch a fight, with tens of millions watching video remotely. With development on the game entering its second week, let's talk about how the player controls their mech during a fight.

The core moves available to every mech are **walking, jumping, and boosting**. Of course, a heavier mech, constructed from heavier parts, will not be able to act as quickly as a lighter mech, so these core moves will vary from build to build.

- **Walking** is the mechanical action common to land animals of walking around on your legs. A mech is probably bipedal, though not especially fast. Their size, weight, and man-made motor functions make them difficult to pilot.
- **Jumping** is a risky maneuver during a fight, especially for something so potentially unwieldy as a mech. It involves using mechanical thrust to propel the mech into the air, as a mech lacks an athlete's organic muscles.
- **Boosting** involves firing boosters or flaring vents in the mech build to propel the mech forward. Boost along the ground, skimming the battlefield, or in the air, extending leaps from high-altitude terrain. Since boosting doesn't involve mechanical propulsion, pilots commonly boost in situations necessitating great agility.

To handle the battle-specific character movement I created a script called **FightControl** and stuck it on a GameObject wrapper with a **MechVisualizer** inside. Usually, when we're working with character controllers, we don't want to work with Unity's physics engine. Even though a physics-based character controller sounds logical, traditional game design thinking suggests that the player character's movement is too central to leave up to a physics engine. We want granular control rather than set-it-and-forget-it physics simulation. On past projects like [Cavalcade](/projects/cavalcade) and [Anemone](/projects/anemone), I've enjoyed subverting this practice by letting physics (Unity RigidBody) play a central role in player interaction. For Future Battle Legend we need to follow the traditional thinking and move the player's Transform position directly.

As for Unity's gravity, we'll leave it in place, but crank its power way up from the default setting. I've always had a hard time working with Unity's gravity simulation. I find the default force of gravity too weak, but once we increase its power I have a hard time honing in on the strength of a jump or similar gravity-defying movement. By the same logic that we don't use the physics system for movement, maybe I'll circle back to gravity later and consider rolling up my own gravity simulation.

Boosting is essentially giving a mech a big burst of speed for a short time. Boost power is calculated from a mech's stats and FightControl ramps up the mech's movement speed in a coroutine, waits a moment, and then eases it back down. The initial challenges with boost were finding the right acceleration and dealing with choppy changes in the mech's speed.

Another aspect of the fight controller deals with actions specific to each mech: **Abilities**. Passive Abilities affect a mech in the background without being triggered by the player, however, the player must tap in Active Abilities to use them. A mech's **Command Menu** stores Active Abilities in four slots that the player can toggle between during battle and trigger with a click. An Active Ability might be, for example, a "Fire" option attached to a rifle Part. The first weapons I made were the Nguyen-Bojin "Scimitar," with a melee attack Ability, and the Nguyen-Bojin "Pistol," with a projectile attack Ability.

At this stage, we can now attach the Parts instantiated by the MechVisualizer onto an animation armature and have FightControl pass cues to the Animator so that our mech can walk around. Triggers for the animations associated with the standard movements and Active Abilities are also served up by FightControl when the player uses their Command Menu. With all these things acting in tandem we have a nice little character controller up and running.

The realtime action in Future Battle Legend will necessitate a long-term calibration of FightControl and possibly revisions on gravity, boost, animation, and so on. This first edition of FightControl will certainly not be the last. Thanks for reading! For more updates on this game's development [follow @avknights_dev on Twitter](https://twitter.com/avknights_dev).
