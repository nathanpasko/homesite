---
title: "How to Write CSS for Both Light & Dark Display Modes"
date: "2021-01-10"
tags: ["getting more responsive", "webdev", "html", "css"]
---

This is the beginning of a little series about responsive design and some cool new ways to make your web projects—sites, apps, games—more responsive. In this article we'll check out a quick and easy way to use CSS media queries to check if the client device is using light or dark display mode, and set different styles for light and dark.

# Why Light/Dark Mode Shifting is Cool

Responsive web design has accelerated to meet the needs of an ecosystem full of different web-capable devices. Designers no longer take screen sizes for granted, so the web has become more fluid as designers create sites that adapt to context. Responsiveness dovetails with the industry standardization of "**Dark Mode**"—that is, the capability of your OS to switch between a light- or dark-toned UI.

I was converted to Dark Mode fandom when I discovered I could set my phone and laptop to switch **automatically** from light to dark at sunset (and back to light at sunrise).

**Tapping into users' light/dark mode setting and presenting a version of our site to match is another powerful form of responsiveness.** From a design standpoint, an OS with these display features is an invitation to create more fluid images and brand identities; the proliferation of these features **encourages designers to express the same identity using multiple color schemes**, where in the past they might've stopped at one. This makes media more comfortable for users and helps images, websites, apps, and brands feel more intelligent and less static.

We can use a CSS media query to set up alternate styles for when users switch their client device between light and dark display modes. Now our site is responsive not only on a basis of screen size, but a basis of color scheme as well. Remember, responsiveness is all about responding to the context that different users and situations bring to our site.

# How to Use `prefers-color-scheme` to Set Styles Specific to Light or Dark Mode

Here's the media query that we can use to tap into the display color scheme, light or dark:

```css
@media (prefers-color-scheme: dark) {
  /* These styles will be applied only in dark mode */
}

@media (prefers-color-scheme: light) {
  /* These styles will be applied only in light mode */
}
```

As you can see, CSS gives us everything we need with media queries. Let's whip up a very basic example to test it out.

Let's start by making a blank HTML page. We just need to add some text to demonstrate the change from light to dark.

```html
<h1>Light vs. Dark</h1>
<h2>Testing</h2>
<h2>Testing</h2>
```

Similar to the way, in working with screen width, we write base styles assuming a very narrow breakpoint and use media queries to adapt to progressively wider breakpoints, we will write some CSS assuming light mode to start. Then we use the `prefers-color-scheme: dark` media query to write corrective styles for dark mode.

```css
/* Default styles */
html {
  /* Black text on a white background */
  color: black;
  background: white;
}

/* Style changes to use in dark mode */
@media (prefers-color-scheme: dark) {
  html {
    /* White text on a black background */
    color: white;
    background: black;
  }
}
```

If we use that CSS on our page and then switch between light and dark display modes... voilà!

This example just inverts text color and background color on our page. That's a basic and pretty common application of light/dark mode responsiveness, but it's pretty useful. Like  the other building blocks of CSS, the possibilities this provides are endless.

# Go Make Something More Responsive

The changes between light and dark mode can be subtle if diluting a brand or aesthetic is too risky, or they can be drastic, putting full emphasis on readability or low-light operation. Responsive design is all about expanding a design's preparedness and creating opportunities for a more specific and intuitive user experience. Using the `prefers-color-scheme` query will help you invest more in your designs and serve users a more engaging experience.