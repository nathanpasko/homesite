---
title: "My First Year with React"
date: "2021-05-16"
tags: ["react", "webdev"]
---

Around this time last year, I was focused more on JavaScript and HTML than any front end framework. I had spent some time learning Angular, but I was also several months removed from working on any Angular project.

# Learning React

So around this time I finally gave in and started learning **React**. My first impression was actually less related to the framework and more to the quality of the React introductory tutorial. I was surprised how easy it was to read and understand.

As I toured through the different areas of the framework, that feeling just expanded to encapsulate how I felt about React on the whole. I love the straightforwardness that seems to permeate React's design.

# Class Components

One thing I was reading about in the documentation but not quite understanding: **Hooks**. Maybe it was the terminology, the explanation, or maybe I just needed more time to wrap my head around everything, but I wasn't sure exactly what I was looking at with Hooks. So I started writing **class components**. This was great in and of itself because even though the docs made it clear that Hooks were the new thing, the docs made it just as clear that they were an optional enhancement to the React framework. I was simply encouraged to come back and check out Hooks when I was ready.

Instead I focused on what seemed the older, more stable core of React. **I got my footing and then shipped my first React app about four months later.** 

# Learning Hooks

Sometime after the launch that project, after I'd had some time to rest, I found myself getting ready to start a new side project (built on React, since I was fascinated). Checking something on Stack Overflow, I saw a comment that reminded me of React Hooks. Finally I doubled back to the React docs to get a clear idea of what those durn Hooks were.

What can I say? I love em. Writing all **functional components** and using a retooled **State** save me code (and clutter). Everything feels lighter on its feet; more streamlined. Maybe I can't truly wish that I'd employed Hooks from the beginning—since their depiction as **optional** was part of the welcoming, practical air that drew me in—but I do feel that Hooks have been a huge improvement to the way I work with React.

# One Year Anniversary

It's been about a year since I started messing around with React, and I like the framework quite a lot. Its popularity led to the release of so many helpful packages, and that goes a long way. Its popularity also makes it easier to learn because there are more people asking and answering questions online, more people creating more learning resources for more people of different learning styles. Today I'm still maintaining React apps for a couple clients, but if I were to start a new side project right now, I might reach for a more lightweight framework. That'd be more out of curiosity than anything else. Like anything you practice regularly for a year, it's easy for me write with React brain right now. Maybe in another year's time, I'll be hooked on a different framework.