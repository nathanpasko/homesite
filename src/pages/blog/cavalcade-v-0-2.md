---
title: "Cavalcade v0.2"
date: "2018-10-25"
tags: ["cavalcade", "devlog", "gamedev"]
---
![screenshot of red door and superimposed hand icon](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODUzNzgucG5n/x200/E2Xwuu.png)

Connected rooms with animated doors. Though the rooms are all identical in shape (square) and size, the "grid" is not completely filled in: there are not 4 doors in every room. When loading a new room, the Mother brain places the decorates the room correctly, adjusts the lights, and loads data about where adjacent rooms are. Doors are activated on those walls. Began adding ambient sounds into the game; working on balancing them so as not to distract from the rest of the environment. Mother brain now loads data from Scriptable Object databases so things like room info and Creat prefabs can sit in blocks in the editor.

![red-lit screenshot of early creature](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODUzNzcucG5n/x200/YPolMw.png)
![dim screenshot of early creature](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODUzNzkucG5n/x200/qDWPnI.png)