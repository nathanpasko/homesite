---
title: "Cavalcade v0.3"
date: "2018-10-27"
tags: ["cavalcade", "devlog"]
---

![screenshot of creature and nodules](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTUucG5n/original/d5w2D2.png)
![screenshot of creature with feet and a candle](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTMucG5n/x200/ZjdpUA.png)

Used a Layer Mask to keep the raycast used by the doors from hitting the player, fixing a bug where doors would flicker between interactable/non-interactable. Added animated "onboarding" info screen with brief instructions to the player re: sound, controls, etc. Ongoing refinement of body physics. the game now contains 12 rooms, which feels like a good number. Final stages of development will include finalizing the layout of the rooms, as well as how objects and lights within the rooms are arranged.

![dim screenshot of large creature partially obscuring the camera](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTQucG5n/x200/sc93pL.png)
![red-lit screenshot of fleshy puddle and emerging nodule](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODk1OTIucG5n/x200/DIR17t.png)
