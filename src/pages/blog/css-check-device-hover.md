---
title: "How to Check Whether a Device Supports Hover with a CSS Media Query"
date: "2021-02-07"
tags: ["getting more responsive", "webdev", "css"]
---

This series is all about **expanding our definition of responsive web design** beyond the commonly-held starting point of handling different screen sizes. Similar to our [discussion of automatically responding to devices' light/dark display settings](https://dev.to/sprite421/how-to-write-css-for-both-light-dark-display-modes-4bok), today we'll once again use a CSS media query to respond to the context presented by client devices.

# Interactivity & Hover

Hover effects are important! We know we should **never** hijack the cursor icon to highlight interactive UI elements like buttons. That compromises the spec and has snowballing accessibility complications. Finding another way to **communicate interactivity** will help users navigate our interfaces effortlessly, so let's use CSS to make our hover effects smarter.

## The Problem

What makes hover effects tricky is the difference between devices with traditional mouse-like pointers, and devices with touch input like smart phones. We don't really have the same continuous pointer input on a touch device that we do when using a mouse, but a site's design should still communicate what elements are interactive.

## A Solution

Today let's solve this problem by communicating interactivity with color. Users with a mouse-like pointer will be able to test an element's interactivity by hovering over it. When they hover, interactive elements will be highlighted with a bright background color. Touch device users won't have that opportunity to hover, so we'll set interactive elements to have the highlighted background color at all times.

# How to Detect Hover Support in CSS

Let's build a basic example to test our solution. We can start with a blank HTML page. We'll add some CSS styles shortly, but first we'll give ourselves an interactive element to play with: a button.

```html
<button>Button</button>
```

Buttons are familiar in function but they take many different forms, so it's important to communicate visually that they're interactive. We'll use a **yellow background** to indicate interactivity.

Like usual, we'll write our stylesheet to default to mobile device styles, and use media queries to make changes in styles for other devices. So let's start by styling `button` to have the yellow background associated with interactivity.

```css
button {
  background: yellow;
}
```

We'll use the `hover` media feature in a query to test whether hover is supported by the client device. Let's add this media query to our stylesheet so that we can apply styles for hover-capable devices.

```css
@media (hover: hover) {
  button {
    background: white;
  }
  button:hover {
    background: yellow;
  }
}
```

Now the capability to hover over buttons will override the default button styles and allow users to test interactivity by hovering. It's that easy! Sad to say I went years without using this helpful media query and was stymied by my hover styles predicament the whole time.

| *Note!* |
|---|
| The `hover` media feature can report either `hover` or `none`. Devices capable of hover report `hover`, and devices incapable of hover report `none`.|

# Get Out There and Use It

This kind of responsiveness is a big deal. I think it's the **highest priority** responsiveness technique we've talked about in this series. It's tied into the rampant **misuse** of the cursor icon reserved for hovering over hyperlinks 👆 and results in a spec-friendly fix to the problem of communicating interactivity. Oh, `@media (hover)`, where have you been all my life?