---
title: "Cavalcade v1.1 and Cavalcade+ Now Available"
date: "2018-11-07"
tags: ["cavalcade", "devlog", "gamedev"]
---

![promotional image of Cavalcade+ creatures](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE2MTkwMTYucG5n/500x/gg6tky.png)

Cavalcade v1.1 and Cavalcade+

Cavalcade debuted in the Haunted PS1 Halloween Jam. Now Cavalcade has been patched up to version 1.1 with some bug fixes, an improved candle, and a bit of retexturing. Cavalcade+ is now available as well. When you download the standard "PS1" version of Cavalcade you can name your own price. Pay $2 or more to unlock Cavalcade+ at the same time. To make Cavalcade+, vanilla Cavalcade was used as a base, but materials and shaders were changed to enable more contemporary mesh rendering and lighting effects. A little more audio was added, and menus were retooled to fit the new "PS2" screen resolution.

Thanks for checking out cavalcade!