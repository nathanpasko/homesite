---
title: "A Few Refinements"
date: "2020-03-22"
tags: ["future battle legend", "devlog", "gamedev"]
---

This week was a light one in terms of development. As COVID-19 (and Animal Crossing: New Horizons) sweep the US, locking down development time is getting difficult. In this post, I'll just detail a few refinements I've made to _Future Battle Legend_'s systems recently.

First is a tighter **field of view** for the camera. Since building a personal mech is one of the main gameplay elements of _Future Battle Legend_, a mech's appearance should get to be front-and-center during play. So we're zooming that camera in! The concern with making the player too big onscreen is that the opponent and terrain will be obscured and difficult to understand. Moving forward, we'll continue to dial into the perfect field of view for the fight camera.

Also dealing with the camera, I've **smoothed out the Lock-On camera about 50%**. The ultimate goal is for the camera not to snap to a new focal point in switching between Free/Lock-On mode, but rather for the camera to tilt without "cutting" to look at its new target. The changes I've made so far bring the Lock-On camera methodology closer to the Free camera behavior. Both camera modes use `Vector3.MoveTowards` to move the camera step by step, rather than the original, instant cut method.

- The **Lock Position** is a calculated position for the camera to assume while Lock-On is active.
- `followSpeed` is a float governs the movement speed of the camera in both Free and Lock-On states.
- We can calculate a step for the camera to move in a single frame by using `Time.deltaTime` in `Vector3.MoveTowards`.
- `Transform.LookAt` aims the camera at the Lock-On target.

At this stage, the cutting is less awkward than it was, but still too jarring, especially if the camera has to rotate a lot to get into position for Lock-On. To smooth this out the rest of the way, I'll try to calculate a transitional camera rotation instead of calling `Transform.LookAt` outright.

Remember the [MechVisualizer](/blog/mech-visualizer)? With a few tweaks, I was able use the MechVisualizer on a second GameObject and voila! Now we have an actual mech to target, not just a grey box. Our opponent doesn't move, but it's just a bit more convincing and fun now.

Behind the scenes in the code, I've restructured some things to make data flow more freely between objects (will I regret this?) and to lay the groundwork for targeting. Currently, I'm prototyping the **targeting** behavior. While the Lock-On feature can be used to focus on your opponent, we still need the targeting feature so that you can quickly change which Part on the enemy mech you want to target. Your strategy might involve disabling their weapons first, or maybe their Tech-heavy parts to cripple their systems. Targeting is still in the very early stages of development, but I'm trying a popup menu setup where all viable parts are displayed onscreen for the player to choose a target.

That's just a mock-up. Maybe next time the targeting will be implemented in a working fashion and we can start working on the UX for that feature. It's a delicate area of gameplay because the player needs to be able to make an informed choice with the data they're given, but they need to be able to make that choice quickly while maintaining a decent view of the battlefield. Stay tuned and we'll start hacking away at this important battle feature. Thanks for reading! For more updates on this game's development [follow @avknights_dev on Twitter](https://twitter.com/avknights_dev).
