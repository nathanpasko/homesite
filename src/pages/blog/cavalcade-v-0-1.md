---
title: "Cavalcade v0.1"
date: "2018-10-24"
tags: ["cavalcade", "devlog", "gamedev"]
---

![Screenshot of early title screen](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODMxODAucG5n/original/7ZgwU4.png)
![Screenshot of first creature](https://img.itch.zone/aW1hZ2UvMzIwODc4LzE1ODMxODEucG5n/original/yE7iSy.png)

first build of the game. basic first-person movement set up. Breathe script added to animate walls, as well as Creat body parts. Objects to decorate the rooms, though they share the same underlying architecture & layout. Springy nodules can be enabled in some rooms; these protrude from the floor and impede the player's navigation somewhat. The player can push around and otherwise interact with objects with RigidBodies. These now include certain Creat body parts as well as the growths out of the floor, but tables, candles, lamps, tape players, etc. could also become interactive. This decision will reflect the flow of gameplay once doors & moving from room to room are more fully implemented.